// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyAOKXGxY_BiQt20pgHhlbvvjffnu7PylAw",
    authDomain: "testreal-84447.firebaseapp.com",
    databaseURL: "https://testreal-84447.firebaseio.com",
    projectId: "testreal-84447",
    storageBucket: "testreal-84447.appspot.com",
    messagingSenderId: "781917638204",
    appId: "1:781917638204:web:a097a26936d28a3aa8bd39",
    measurementId: "G-RNBJRW6BEQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
