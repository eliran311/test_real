import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-saveposts',
  templateUrl: './saveposts.component.html',
  styleUrls: ['./saveposts.component.css']
})
export class SavepostsComponent implements OnInit {
  userId;
  classify$:Observable<any>;
  likes = 0;
  constructor(public postsService:PostsService,public authService:AuthService) { }
  addLikes(){
    this.likes++
  }
  ngOnInit() {
    
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
     this.classify$ = this.postsService.getPosts(this.userId);
  })

}
deletePost(id:string){
  this.postsService.deletePost(this.userId,id);

}
}
