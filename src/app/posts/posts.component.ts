import { Comments } from './../interfaces/comments';
import { PostsService } from './../posts.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$:Observable<Posts[]>  
  comments$:Observable<Comments[]>
  
  // posts$: Posts[]=[];
  //  comments$: Comments[]=[];
  userId:string;
  body:string;
  title:string;
  number:number;
  pushAlert:string;
  id:number;
  constructor(private postsService:PostsService,public authService:AuthService,private router: Router,private route:ActivatedRoute ) { }
  
  
  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
    
  })
    this.posts$= this.postsService.getposts();
    this.comments$= this.postsService.getcomments();
    console.log(this.body)
    // this.postsService.getposts().subscribe(data => this.posts$ = data);
    // this.postsService.getcomments().subscribe(data => this.comments$ = data);
  }
  push(){
  console.log(this.userId);
    this.postsService.addPost(this.userId,this.body,this.title);
   this.router.navigate(['/Savepost']);
   }

}
