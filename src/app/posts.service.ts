import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule  } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Posts } from './interfaces/posts';
import { map } from 'rxjs/internal/operators/map';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Comments } from './interfaces/comments';
@Injectable({
  providedIn: 'root'
})
export class PostsService {
 
  

 userCollection:AngularFirestoreCollection = this.db.collection('users');
 postCollection:AngularFirestoreCollection
private URL = "https://jsonplaceholder.typicode.com/posts/";
private URL1 ="https://jsonplaceholder.typicode.com/comments";

  constructor(private http: HttpClient,  private db:AngularFirestore,private authService:AuthService) { }
  getposts():Observable<Posts[]> {
    return this.http.get(this.URL).pipe(map((response:Posts[]) => response));
         
  };
  getcomments():Observable<Comments[]> {
    return this.http.get(this.URL1).pipe(map((response:Comments[]) => response));
         
  };
  addPosts(title:string, body:string){
    const post = {title:title, body:body}
   //  this.db.collection('posts').add(post);
   this.db.collection('posts').add(post);
 }

 getPosts(userId): Observable<any[]>{
  this.postCollection = this.db.collection(`users/${userId}/posts`);
  console.log('Posts collection created');
  // return this.db.collection('posts').valueChanges(({idField:'id'}));
  return this.postCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      return data;
    }))
  );
   }
   getPost(userId,id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()
  }

  deletePost(userId,id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  addPost(userId,title:string, body:string){
    const post = {title:title, body:body}
   //  this.db.collection('posts').add(post);
    this.userCollection.doc(userId).collection('posts').add(post);
 }
}
